package main

import (
	"fmt"
	"sync"
)

func doWorker(id int,c chan int,wg *sync.WaitGroup) {
	for n := range c {
		fmt.Printf("worked %d recrived %c\n",id,n)
		wg.Done()
	}
}

type worker struct {
	in chan int
	wg *sync.WaitGroup
}

func createWorker(id int,wg *sync.WaitGroup) worker {
	w := worker{
		in : make(chan int),
		//done : make(chan bool),
		wg : wg,
	}
	go doWorker(id,w.in,wg)
	return w
}

func chanDemo() {
	var workers [10]worker
	var wg sync.WaitGroup

	for i := 0;i<10;i++ {
		workers[i] = createWorker(i,&wg)
	}
	wg.Add(20)
	for i,worker := range workers{
		worker.in <- 'a'+i
		//<-workers[i].done
	}
	for i,worker := range workers{
		worker.in <- 'A'+i
		//<-workers[i].done
	}

	wg.Wait()
}


func main() {
	chanDemo()

}
