package main

import (
	"fmt"
	"net"
)

func main() {
	//listen, err := net.Listen("tcp4","0.0.0.0:9501")
	listen, err := net.ListenTCP("tcp4",&net.TCPAddr{
		IP:net.IPv4(0,0,0,0),
		Port: 9501,
	})
	if err != nil{
		fmt.Println("=======listen err ====", err)
		return
	}
	defer listen.Close()
	fmt.Println("=========listen success========",listen)
	for {

		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("=======accept err ====", err)
			continue
		}
		fmt.Println("=========accept success========",conn)
		defer conn.Close()
		var msg [3]byte
		msg[0] = 0x41
		msg[1] = 0x42
		msg[2] = 0x43
		conn.Write(msg[:])
	}

}