#include<stdio.h>
#include<sys/socket.h>
#include<errno.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>

// struct sockaddr_in {
//     sa_family_t    sin_family; /* address family: AF_INET */
//     in_port_t      sin_port;   /* port in network byte order */
//     struct in_addr sin_addr;   /* internet address */
// };

// /* Internet address. */
// struct in_addr {
//     uint32_t       s_addr;     /* address in network byte order */
// };

int main(){
    int fd = socket(AF_INET,SOCK_STREAM,0);
    if(fd == -1){
        printf("==========err=%d===%s==\r\n",errno,strerror(errno));
        return 0;
    }
    printf("==========main==start==%d==\r\n",fd);
    struct sockaddr_in address,clientaddress;
    address.sin_family = AF_INET;
    address.sin_port = 0xc619;
    address.sin_addr.s_addr = INADDR_ANY;

    socklen_t addlen = sizeof(address);
    socklen_t clientaddlen = sizeof(clientaddress);
    int ret = bind(fd,(struct sockaddr *)(&address),addlen);
    if(ret != 0){
        printf("=======bind===err=%d===%s==\r\n",errno,strerror(errno));
        return 0;
    }

    ret = listen(fd, 128);
    if(ret != 0){
        printf("=======listen===err=%d===%s==\r\n",errno,strerror(errno));
        return 0;
    }

    int connfd = accept(fd, (struct sockaddr *)(&clientaddress), &clientaddlen);
    if(connfd > 0){
        printf("=======accept===recv=%d=====\r\n",connfd);
        write(connfd,"China",5);
    }
    printf("==========main=close=connfd==%d=%d=\r\n",connfd,fd);
    close(connfd);
    close(fd);
    return 0;
}