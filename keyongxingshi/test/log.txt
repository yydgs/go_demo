[root@localhost keyongxingshi]# file demo1
demo1: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), not stripped
[root@localhost keyongxingshi]# ldd demo1
        linux-vdso.so.1 =>  (0x00007fff01d2f000)
        libpthread.so.0 => /lib64/libpthread.so.0 (0x00007feb140bf000)
        libc.so.6 => /lib64/libc.so.6 (0x00007feb13cf1000)
        /lib64/ld-linux-x86-64.so.2 (0x00007feb142db000)
[root@localhost keyongxingshi]# nm /lib64/
Display all 2209 possibilities? (y or n)
[root@localhost keyongxingshi]# nm /lib64/libc
Display all 124 possibilities? (y or n)
[root@localhost keyongxingshi]# nm /lib64/libc-2.17.so
demo1     demo1.go  .idea/
[root@localhost keyongxingshi]# nm /lib64/libc-2.17.so | grep listen
00000000000ff910 t __GI_listen
00000000000ff910 t __GI___listen
00000000000ff910 W listen
00000000000ff910 t __listen
[root@localhost keyongxingshi]# nm /lib64/libc-2.17.so | grep accept
00000000000ff790 W accept
00000000000ff790 t __accept
00000000000fff30 T accept4
00000000000ff799 t __accept_nocancel
00000000000cf900 t check_node_accept
00000000000d14f0 t check_node_accept_bytes.isra.25
00000000000ff790 t __GI_accept
00000000000ff790 t __GI___accept
00000000000ff790 t __GI___libc_accept
0000000000128540 t __GI_xdr_accepted_reply
00000000003ccd18 b IO_accept_foreign_vtables
00000000000ff790 t __libc_accept
00000000001332f0 t __svc_accept_failed
0000000000128540 T xdr_accepted_reply
[root@localhost keyongxingshi]# nm /lib64/libc-2.17.so | grep epoll_create
00000000000ff020 T epoll_create
00000000000ff050 T epoll_create1
00000000000ff020 t __GI_epoll_create
00000000000ff050 t __GI_epoll_create1
[root@localhost keyongxingshi]# nm /lib64/libc-2.17.so | grep epoll_wait
00000000000ff0b0 T epoll_wait
00000000000ff0b9 t __epoll_wait_nocancel
00000000000ff0b0 t __GI_epoll_wait
[root@localhost keyongxingshi]# nm /lib64/libc-2.17.so | grep poll
00000000000ff020 T epoll_create
00000000000ff050 T epoll_create1
00000000000ff080 T epoll_ctl
00000000000fecc0 T epoll_pwait
00000000000ff0b0 T epoll_wait
00000000000ff0b9 t __epoll_wait_nocancel
00000000000ff020 t __GI_epoll_create
00000000000ff050 t __GI_epoll_create1
00000000000ff080 t __GI_epoll_ctl
00000000000fecc0 t __GI_epoll_pwait
00000000000ff0b0 t __GI_epoll_wait
00000000000f3db0 t __GI___libc_poll
00000000000f3db0 t __GI_poll
00000000000f3db0 t __GI___poll
00000000000f3e10 t __GI_ppoll
0000000000132750 t __GI___rpc_thread_svc_max_pollfd
0000000000132720 t __GI___rpc_thread_svc_pollfd
00000000001330e0 t __GI_svc_getreq_poll
00000000000f3db0 t __libc_poll
00000000000f3db0 W poll
00000000000f3db0 W __poll
0000000000118710 T __poll_chk
00000000000f3db9 t __poll_nocancel
00000000000f3e10 T ppoll
0000000000118730 T __ppoll_chk
0000000000132750 T __rpc_thread_svc_max_pollfd
0000000000132720 T __rpc_thread_svc_pollfd
00000000001330e0 T svc_getreq_poll
00000000003cd0a0 B svc_max_pollfd
00000000003cd0a8 B svc_pollfd
[root@localhost keyongxingshi]#





[root@localhost keyongxingshi]# ldd demo1
        linux-vdso.so.1 =>  (0x00007ffcd49ed000)
        libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f2a102f9000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f2a0ff2b000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f2a10515000)
[root@localhost keyongxingshi]# file /lib64/libpthread.so.0
/lib64/libpthread.so.0: symbolic link to `libpthread-2.17.so'
[root@localhost keyongxingshi]# file /lib64/libth
libthai.so.0           libtheoradec.so.1      libtheoraenc.so.1      libtheora.so.0         libthread_db-1.0.so    libthread_db.so.1      libthunarx-2.so.0.0.0
libthai.so.0.1.6       libtheoradec.so.1.1.4  libtheoraenc.so.1.1.2  libtheora.so.0.3.10    libthread_db.so        libthunarx-2.so.0
[root@localhost keyongxingshi]# file /lib64/libth
libthai.so.0           libtheoradec.so.1      libtheoraenc.so.1      libtheora.so.0         libthread_db-1.0.so    libthread_db.so.1      libthunarx-2.so.0.0.0
libthai.so.0.1.6       libtheoradec.so.1.1.4  libtheoraenc.so.1.1.2  libtheora.so.0.3.10    libthread_db.so        libthunarx-2.so.0
[root@localhost keyongxingshi]# file /lib64/libpthread-2.17.so
/lib64/libpthread-2.17.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), BuildID[sha1]=e10cc8f2b932fc3daeda22f8dac5ebb969524e5b, for GNU/Linux 2.6.32, not stripped
[root@localhost keyongxingshi]#



[root@localhost keyongxingshi]# nm /lib64/libpthread-2.17.so | grep pthread_create
0000000000008120 t __pthread_create_2_1
0000000000008120 T pthread_create@@GLIBC_2.2.5
[root@localhost keyongxingshi]# nm /lib64/libpthread-2.17.so | grep pthread_join
0000000000008f70 T pthread_join
[root@localhost keyongxingshi]#

-------------------------------------------------系统调用----------------------------------------------
[root@localhost keyongxingshi]# ./demo1
&{0xc000108000 {<nil> 0}}
[root@localhost keyongxingshi]# echo $$
64731
[root@localhost keyongxingshi]# ./demo1
&{0xc000108000 {<nil> 0}}
[root@localhost keyongxingshi]#

[root@localhost ~]# strace -f -s 6598 -e trace=clone,execve,socket,bind,listen -p 64731
strace: Process 64731 attached
socket(AF_NETLINK, SOCK_RAW, NETLINK_AUDIT) = 3
clone(strace: Process 65378 attached
child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7fa51a10da10) = 65378
[pid 65378] execve("./demo1", ["./demo1"], 0xaa5a20 /* 27 vars */) = 0
[pid 65378] clone(strace: Process 65379 attached
child_stack=0x7fe4f6b7bfb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe4f6b7c9d0, tls=0x7fe4f6b7c700, child_tidptr=0x7fe4f6b7c9d0) = 65379
[pid 65378] clone(child_stack=0x7fe4f637afb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe4f637b9d0, tls=0x7fe4f637b700, child_tidptr=0x7fe4f637b9d0) = 65380
strace: Process 65380 attached
[pid 65378] clone(strace: Process 65381 attached
child_stack=0x7fe4f5b79fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe4f5b7a9d0, tls=0x7fe4f5b7a700, child_tidptr=0x7fe4f5b7a9d0) = 65381
[pid 65380] clone(strace: Process 65382 attached
child_stack=0x7fe4f5378fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe4f53799d0, tls=0x7fe4f5379700, child_tidptr=0x7fe4f53799d0) = 65382
[pid 65378] clone(strace: Process 65383 attached
child_stack=0x7fe4f4b77fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe4f4b789d0, tls=0x7fe4f4b78700, child_tidptr=0x7fe4f4b789d0) = 65383
[pid 65378] clone(strace: Process 65384 attached
child_stack=0x7fe4efffefb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe4effff9d0, tls=0x7fe4effff700, child_tidptr=0x7fe4effff9d0) = 65384
[pid 65378] socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_IP) = 3
[pid 65378] bind(3, {sa_family=AF_INET, sin_port=htons(6598), sin_addr=inet_addr("0.0.0.0")}, 16) = 0
[pid 65378] listen(3, 128)              = 0
[pid 65380] +++ exited with 0 +++
[pid 65382] +++ exited with 0 +++
[pid 65379] +++ exited with 0 +++
[pid 65384] +++ exited with 0 +++
[pid 65383] +++ exited with 0 +++
[pid 65381] +++ exited with 0 +++
[pid 65378] +++ exited with 0 +++
--- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=65378, si_uid=0, si_status=0, si_utime=0, si_stime=1} ---






[root@localhost ~]# strace -f -s 6598 -e trace=clone,execve,socket,bind,listen -p 3217
strace: Process 3217 attached
socket(AF_NETLINK, SOCK_RAW, NETLINK_AUDIT) = 3
clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f208f250a10) = 3601
strace: Process 3601 attached
[pid  3601] execve("./demo1", ["./demo1"], 0x12f5020 /* 27 vars */) = 0
[pid  3601] clone(child_stack=0x7f3e14ad6fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f3e14ad79d0, tls=0x7f3e14ad7700, child_tidptr=0x7f3e14ad79d0) = 3602
[pid  3601] clone(strace: Process 3602 attached
strace: Process 3603 attached
child_stack=0x7f3e142d5fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f3e142d69d0, tls=0x7f3e142d6700, child_tidptr=0x7f3e142d69d0) = 3603
[pid  3601] clone(strace: Process 3604 attached
child_stack=0x7f3e13ad4fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f3e13ad59d0, tls=0x7f3e13ad5700, child_tidptr=0x7f3e13ad59d0) = 3604
[pid  3603] clone( <unfinished ...>
[pid  3601] clone(strace: Process 3606 attached
strace: Process 3605 attached
 <unfinished ...>
[pid  3603] <... clone resumed>child_stack=0x7f3e12ad2fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f3e12ad39d0, tls=0x7f3e12ad3700, child_tidptr=0x7f3e12ad39d0) = 3605
[pid  3601] <... clone resumed>child_stack=0x7f3e132d3fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f3e132d49d0, tls=0x7f3e132d4700, child_tidptr=0x7f3e132d49d0) = 3606
[pid  3605] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3601, si_uid=0} ---
[pid  3601] socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_TCP) = 3
[pid  3601] socket(AF_INET6, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_TCP) = 3
[pid  3601] bind(3, {sa_family=AF_INET6, sin6_port=htons(0), inet_pton(AF_INET6, "::1", &sin6_addr), sin6_flowinfo=htonl(0), sin6_scope_id=0}, 28) = 0
[pid  3601] socket(AF_INET6, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_TCP) = 4
[pid  3601] bind(4, {sa_family=AF_INET6, sin6_port=htons(0), inet_pton(AF_INET6, "::ffff:127.0.0.1", &sin6_addr), sin6_flowinfo=htonl(0), sin6_scope_id=0}, 28) = 0
[pid  3601] socket(AF_INET6, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_IP) = 3
[pid  3601] bind(3, {sa_family=AF_INET6, sin6_port=htons(6598), inet_pton(AF_INET6, "::", &sin6_addr), sin6_flowinfo=htonl(0), sin6_scope_id=0}, 28) = 0
[pid  3601] listen(3, 128)              = 0
[pid  3603] +++ exited with 0 +++
[pid  3605] +++ exited with 0 +++
[pid  3602] +++ exited with 0 +++
[pid  3606] +++ exited with 0 +++
[pid  3604] +++ exited with 0 +++
[pid  3601] +++ exited with 0 +++
--- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=3601, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
^Cstrace: Process 3217 detached
[root@localhost ~]# strace -f -s 6598 -e trace=clone,execve,socket,bind,listen,epoll_ctl,setspckopt -p 3217
strace: invalid system call 'setspckopt'
[root@localhost ~]# strace -f -s 6598 -e trace=clone,execve,socket,bind,listen,epoll_ctl,setsockopt -p 3217
strace: Process 3217 attached
socket(AF_NETLINK, SOCK_RAW, NETLINK_AUDIT) = 3
clone(strace: Process 3639 attached
child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f208f250a10) = 3639
[pid  3639] execve("./demo1", ["./demo1"], 0x12f5020 /* 27 vars */) = 0
[pid  3639] clone(child_stack=0x7fe7e9c37fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe7e9c389d0, tls=0x7fe7e9c38700, child_tidptr=0x7fe7e9c389d0) = 3640
strace: Process 3640 attached
[pid  3639] clone(strace: Process 3641 attached
child_stack=0x7fe7e9436fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe7e94379d0, tls=0x7fe7e9437700, child_tidptr=0x7fe7e94379d0) = 3641
[pid  3639] clone(strace: Process 3642 attached
child_stack=0x7fe7e8c35fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe7e8c369d0, tls=0x7fe7e8c36700, child_tidptr=0x7fe7e8c369d0) = 3642
[pid  3641] clone(child_stack=0x7fe7e3ffefb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe7e3fff9d0, tls=0x7fe7e3fff700, child_tidptr=0x7fe7e3fff9d0) = 3643
strace: Process 3643 attached
[pid  3639] clone(strace: Process 3644 attached
child_stack=0x7fe7e37fdfb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7fe7e37fe9d0, tls=0x7fe7e37fe700, child_tidptr=0x7fe7e37fe9d0) = 3644
[pid  3639] socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_TCP) = 3
[pid  3639] socket(AF_INET6, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_TCP) = 3
[pid  3639] setsockopt(3, SOL_IPV6, IPV6_V6ONLY, [1], 4) = 0
[pid  3639] bind(3, {sa_family=AF_INET6, sin6_port=htons(0), inet_pton(AF_INET6, "::1", &sin6_addr), sin6_flowinfo=htonl(0), sin6_scope_id=0}, 28) = 0
[pid  3639] socket(AF_INET6, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_TCP) = 4
[pid  3639] setsockopt(4, SOL_IPV6, IPV6_V6ONLY, [0], 4) = 0
[pid  3639] bind(4, {sa_family=AF_INET6, sin6_port=htons(0), inet_pton(AF_INET6, "::ffff:127.0.0.1", &sin6_addr), sin6_flowinfo=htonl(0), sin6_scope_id=0}, 28) = 0
[pid  3639] socket(AF_INET6, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_IP) = 3
[pid  3639] setsockopt(3, SOL_IPV6, IPV6_V6ONLY, [0], 4) = 0
[pid  3639] epoll_ctl(5, EPOLL_CTL_ADD, 6, {EPOLLIN, {u32=6160672, u64=6160672}}) = 0
[pid  3639] epoll_ctl(5, EPOLL_CTL_ADD, 4, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3896487672, u64=140634010660600}}) = 0
[pid  3639] epoll_ctl(5, EPOLL_CTL_DEL, 4, 0xc00004fa24) = 0
[pid  3639] setsockopt(3, SOL_SOCKET, SO_REUSEADDR, [1], 4) = 0
[pid  3639] bind(3, {sa_family=AF_INET6, sin6_port=htons(6598), inet_pton(AF_INET6, "::", &sin6_addr), sin6_flowinfo=htonl(0), sin6_scope_id=0}, 28) = 0
[pid  3639] listen(3, 128)              = 0
[pid  3639] epoll_ctl(5, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3896487672, u64=140634010660600}}) = 0
[pid  3641] +++ exited with 0 +++
[pid  3640] +++ exited with 0 +++
[pid  3643] +++ exited with 0 +++
[pid  3644] +++ exited with 0 +++
[pid  3642] +++ exited with 0 +++
[pid  3639] +++ exited with 0 +++
--- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=3639, si_uid=0, si_status=0, si_utime=0, si_stime=2} ---
^Cstrace: Process 3217 detached
[root@localhost ~]# strace -f -s 6598 -e trace=clone,execve,socket,bind,listen,epoll_ctl,setsockopt -p 3217
strace: Process 3217 attached
socket(AF_NETLINK, SOCK_RAW, NETLINK_AUDIT) = 3
clone(strace: Process 3749 attached
child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f208f250a10) = 3749
[pid  3749] execve("./demo1", ["./demo1"], 0x12f5020 /* 27 vars */) = 0
[pid  3749] clone(strace: Process 3750 attached
child_stack=0x7f4f1ffc5fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4f1ffc69d0, tls=0x7f4f1ffc6700, child_tidptr=0x7f4f1ffc69d0) = 3750
[pid  3749] clone(strace: Process 3751 attached
child_stack=0x7f4f1f7c4fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4f1f7c59d0, tls=0x7f4f1f7c5700, child_tidptr=0x7f4f1f7c59d0) = 3751
[pid  3749] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3749, si_uid=0} ---
[pid  3749] clone(strace: Process 3752 attached
child_stack=0x7f4f16fc3fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4f16fc49d0, tls=0x7f4f16fc4700, child_tidptr=0x7f4f16fc49d0) = 3752
[pid  3749] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3749, si_uid=0} ---
[pid  3751] clone(strace: Process 3753 attached
child_stack=0x7f4f1efc3fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4f1efc49d0, tls=0x7f4f1efc4700, child_tidptr=0x7f4f1efc49d0) = 3753
[pid  3749] clone(strace: Process 3754 attached
child_stack=0x7f4f1e7c2fb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4f1e7c39d0, tls=0x7f4f1e7c3700, child_tidptr=0x7f4f1e7c39d0) = 3754
[pid  3749] socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_IP) = 3
[pid  3749] epoll_ctl(5, EPOLL_CTL_ADD, 6, {EPOLLIN, {u32=6160672, u64=6160672}}) = 0
[pid  3749] epoll_ctl(5, EPOLL_CTL_ADD, 4, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=1200762504, u64=139978479906440}}) = 0
[pid  3749] epoll_ctl(5, EPOLL_CTL_DEL, 4, 0xc00004aa24) = 0
[pid  3749] setsockopt(3, SOL_SOCKET, SO_REUSEADDR, [1], 4) = 0
[pid  3749] bind(3, {sa_family=AF_INET, sin_port=htons(6598), sin_addr=inet_addr("0.0.0.0")}, 16) = 0
[pid  3749] listen(3, 128)              = 0
[pid  3749] epoll_ctl(5, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=1200762504, u64=139978479906440}}) = 0
[pid  3750] +++ exited with 0 +++
[pid  3753] +++ exited with 0 +++
[pid  3752] +++ exited with 0 +++
[pid  3751] +++ exited with 0 +++
[pid  3754] +++ exited with 0 +++
[pid  3749] +++ exited with 0 +++
--- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=3749, si_uid=0, si_status=0, si_utime=0, si_stime=1} ---



[root@localhost ~]# strace -f -s 6598 -e trace=clone,execve,socket,bind,listen,epoll_ctl,setsockopt -p 3217
strace: Process 3217 attached
socket(AF_NETLINK, SOCK_RAW, NETLINK_AUDIT) = 3
clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f208f250a10) = 3914
strace: Process 3914 attached
[pid  3914] execve("./demo1", ["./demo1"], 0x12f5020 /* 27 vars */) = 0
[pid  3914] clone(child_stack=0x7f4ffd69ffb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4ffd6a09d0, tls=0x7f4ffd6a0700, child_tidptr=0x7f4ffd6a09d0) = 3915
strace: Process 3915 attached
[pid  3914] clone(strace: Process 3916 attached
child_stack=0x7f4ffce9efb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4ffce9f9d0, tls=0x7f4ffce9f700, child_tidptr=0x7f4ffce9f9d0) = 3916
[pid  3914] clone(child_stack=0x7f4ffc69dfb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4ffc69e9d0, tls=0x7f4ffc69e700, child_tidptr=0x7f4ffc69e9d0) = 3917
strace: Process 3917 attached
[pid  3916] clone(child_stack=0x7f4ffbe9cfb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4ffbe9d9d0, tls=0x7f4ffbe9d700, child_tidptr=0x7f4ffbe9d9d0) = 3918
strace: Process 3918 attached
[pid  3914] clone(child_stack=0x7f4ffb69bfb0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f4ffb69c9d0, tls=0x7f4ffb69c700, child_tidptr=0x7f4ffb69c9d0) = 3919
strace: Process 3919 attached
[pid  3914] socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_IP) = 3
[pid  3914] epoll_ctl(5, EPOLL_CTL_ADD, 6, {EPOLLIN, {u32=6160672, u64=6160672}}) = 0
[pid  3914] epoll_ctl(5, EPOLL_CTL_ADD, 4, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=4209381112, u64=139981488525048}}) = 0
[pid  3914] epoll_ctl(5, EPOLL_CTL_DEL, 4, 0xc00004fa24) = 0
[pid  3914] setsockopt(3, SOL_SOCKET, SO_REUSEADDR, [1], 4) = 0
[pid  3914] bind(3, {sa_family=AF_INET, sin_port=htons(6598), sin_addr=inet_addr("0.0.0.0")}, 16) = 0
[pid  3914] listen(3, 128)              = 0
[pid  3914] epoll_ctl(5, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=4209381112, u64=139981488525048}}) = 0
[pid  3916] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3914, si_uid=0} ---
[pid  3914] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3914, si_uid=0} ---
[pid  3916] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3914, si_uid=0} ---
[pid  3914] --- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=3914, si_uid=0} ---


[root@localhost ~]# netstat -luntp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:6598            0.0.0.0:*               LISTEN      3914/./demo1
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      753/rpcbind
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      1545/dnsmasq
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1195/sshd
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1196/cupsd
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      1488/master
tcp6       0      0 :::111                  :::*                    LISTEN      753/rpcbind
tcp6       0      0 :::22                   :::*                    LISTEN      1195/sshd
tcp6       0      0 ::1:631                 :::*                    LISTEN      1196/cupsd
tcp6       0      0 ::1:25                  :::*                    LISTEN      1488/master
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           766/avahi-daemon: r
udp        0      0 0.0.0.0:40523           0.0.0.0:*                           766/avahi-daemon: r
udp        0      0 192.168.122.1:53        0.0.0.0:*                           1545/dnsmasq
udp        0      0 0.0.0.0:67              0.0.0.0:*                           1545/dnsmasq
udp        0      0 0.0.0.0:68              0.0.0.0:*                           3094/dhclient
udp        0      0 0.0.0.0:111             0.0.0.0:*                           753/rpcbind
udp        0      0 0.0.0.0:177             0.0.0.0:*                           1215/lightdm
udp        0      0 0.0.0.0:924             0.0.0.0:*                           753/rpcbind
udp6       0      0 :::111                  :::*                                753/rpcbind
udp6       0      0 :::177                  :::*                                1215/lightdm
udp6       0      0 :::924                  :::*                                753/rpcbind
[root@localhost ~]# cd /proc/3914
[root@localhost 3914]# ls
attr       cgroup      comm             cwd      fd       io        map_files  mountinfo   net        oom_adj        pagemap      projid_map  schedstat  smaps  statm    task     wchan
autogroup  clear_refs  coredump_filter  environ  fdinfo   limits    maps       mounts      ns         oom_score      patch_state  root        sessionid  stack  status   timers
auxv       cmdline     cpuset           exe      gid_map  loginuid  mem        mountstats  numa_maps  oom_score_adj  personality  sched       setgroups  stat   syscall  uid_map
[root@localhost 3914]# cd fd
[root@localhost fd]# ll
total 0
lrwx------. 1 root root 64 Sep 16 21:57 0 -> /dev/pts/1
lrwx------. 1 root root 64 Sep 16 21:57 1 -> /dev/pts/1
lrwx------. 1 root root 64 Sep 16 21:57 2 -> /dev/pts/1
lrwx------. 1 root root 64 Sep 16 21:57 3 -> socket:[42654]
lrwx------. 1 root root 64 Sep 16 21:57 5 -> anon_inode:[eventpoll]
lr-x------. 1 root root 64 Sep 16 21:57 6 -> pipe:[42655]
l-wx------. 1 root root 64 Sep 16 21:57 7 -> pipe:[42655]
[root@localhost fd]#
[root@localhost fd]#
[root@localhost fd]#
[root@localhost fd]#
[root@localhost fd]# CD ..
bash: CD: command not found...
Similar command is: 'cd'
[root@localhost fd]# cd ..
[root@localhost 3914]# ls
attr       cgroup      comm             cwd      fd       io        map_files  mountinfo   net        oom_adj        pagemap      projid_map  schedstat  smaps  statm    task     wchan
autogroup  clear_refs  coredump_filter  environ  fdinfo   limits    maps       mounts      ns         oom_score      patch_state  root        sessionid  stack  status   timers
auxv       cmdline     cpuset           exe      gid_map  loginuid  mem        mountstats  numa_maps  oom_score_adj  personality  sched       setgroups  stat   syscall  uid_map
[root@localhost 3914]# cd net/
[root@localhost net]# ls
anycast6   dev_mcast     icmp           ip6_mr_cache        ip_mr_cache        ipv6_route  netlink              protocols  rfcomm     rt_cache  sockstat6     udp       wireless
arp        dev_snmp6     if_inet6       ip6_mr_vif          ip_mr_vif          l2cap       netstat              psched     route      sco       softnet_stat  udp6      xfrm_stat
bnep       fib_trie      igmp           ip6_tables_matches  ip_tables_matches  mcfilter    nf_conntrack         ptype      rpc        snmp      stat          udplite
connector  fib_triestat  igmp6          ip6_tables_names    ip_tables_names    mcfilter6   nf_conntrack_expect  raw        rt6_stats  snmp6     tcp           udplite6
dev        hci           ip6_flowlabel  ip6_tables_targets  ip_tables_targets  netfilter   packet               raw6       rt_acct    sockstat  tcp6          unix
[root@localhost net]# cat tcp
  sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode
   0: 00000000:19C6 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 42654 1 ffff88a92dd2f440 100 0 0 10 0
   1: 00000000:006F 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 20176 1 ffff88a936300000 100 0 0 10 0
   2: 017AA8C0:0035 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 29871 1 ffff88a92dd28f80 100 0 0 10 0
   3: 00000000:0016 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 27855 1 ffff88a9363007c0 100 0 0 10 0
   4: 0100007F:0277 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 27364 1 ffff88a92dd28000 100 0 0 10 0
   5: 0100007F:0019 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 27602 1 ffff88a92dd287c0 100 0 0 10 0
   6: 8040A8C0:0016 0140A8C0:C612 01 00000000:00000000 02:0008FD5F 00000000     0        0 42110 2 ffff88a92dd2cd80 21 4 17 10 -1
   7: 8040A8C0:0016 0140A8C0:C5A1 01 00000000:00000000 02:0008B092 00000000     0        0 37864 2 ffff88a92dd2c5c0 21 4 13 10 -1
   8: 8040A8C0:0016 0140A8C0:C639 01 00000000:00000000 02:00093092 00000000     0        0 42297 2 ffff88a92dd2dd00 22 4 29 10 -1
   9: 8040A8C0:0016 0140A8C0:C6C8 01 00000030:00000000 01:00000018 00000000     0        0 44396 4 ffff88a92dd2ec80 24 4 31 10 -1
  10: 8040A8C0:0016 0140A8C0:C597 01 00000000:00000000 02:00089D5F 00000000     0        0 40559 2 ffff88a92dd2b640 20 4 30 10 42
  11: 8040A8C0:0016 0140A8C0:C63D 01 00000000:00000000 02:00093092 00000000     0        0 42313 2 ffff88a92dd2e4c0 21 4 19 10 -1
  12: 8040A8C0:0016 0140A8C0:C6C9 01 00000000:00000000 02:000A9D5F 00000000     0        0 44425 2 ffff88a92dd2ae80 21 4 13 10 -1
  13: 8040A8C0:0016 0140A8C0:C613 01 00000000:00000000 02:0008FD5F 00000000     0        0 42129 2 ffff88a92dd2d540 21 4 0 10 -1
  14: 8040A8C0:0016 0140A8C0:C5A0 01 00000000:00000000 02:0008B092 00000000     0        0 37847 2 ffff88a92dd2be00 23 4 29 10 -1
[root@localhost net]#
