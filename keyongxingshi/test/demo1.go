package main

import (
	"fmt"
	"net"
	"time"
)

func main(){
	listen, err := net.Listen("tcp4","0.0.0.0:6598")
	//socket-->bind->listen
	//进程/net、tcp表
	//一个网络连接4组数据表示：本地ip:port--------》远端ip:port

	//[root@localhost net]# cat tcp
	//sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode
	//0: 00000000:19C6 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 42654 1 ffff88a92dd2f440 100 0 0 10 0

	//[pid  3749] socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, IPPROTO_IP) = 3
	//[pid  3749] epoll_ctl(5, EPOLL_CTL_ADD, 6, {EPOLLIN, {u32=6160672, u64=6160672}}) = 0
	//[pid  3749] epoll_ctl(5, EPOLL_CTL_ADD, 4, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=1200762504, u64=139978479906440}}) = 0
	//[pid  3749] epoll_ctl(5, EPOLL_CTL_DEL, 4, 0xc00004aa24) = 0
	//[pid  3749] setsockopt(3, SOL_SOCKET, SO_REUSEADDR, [1], 4) = 0
	//[pid  3749] bind(3, {sa_family=AF_INET, sin_port=htons(6598), sin_addr=inet_addr("0.0.0.0")}, 16) = 0
	//[pid  3749] listen(3, 128)              = 0
	//[pid  3749] epoll_ctl(5, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=1200762504, u64=139978479906440}}) = 0

	//socket(AF_NETLINK, SOCK_RAW, NETLINK_AUDIT) = 3
	//clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f208f250a10) = 3914
	if err != nil{
		fmt.Println("listern err: ", err)
		return
	}

	fmt.Println(listen)


	conn, err := listen.Accept()
	if err != nil{
		fmt.Println("======", err)
	}
	fmt.Println("========connect=======",conn)
	//conn.Read()
	for{
		time.Sleep(time.Second)
	}
}