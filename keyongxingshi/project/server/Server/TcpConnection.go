package Server

import (
	"fmt"
	"net"
)

type Tcpconnection struct {
	 Conn net.Conn
	 ProtocolName string
}

func MakeClient(conn net.Conn, protocolName string) (client *Tcpconnection, err error){
	client = &Tcpconnection{
		Conn: conn,
		ProtocolName: protocolName,
	}
	return
}

func (this *Tcpconnection)HandMessage(){
	for {
		var msg [10]byte
		this.Conn.Read(msg[:])
		fmt.Println(msg[:])
	}
}