package main

import (
	"fmt"
	//"keyongxingshi/project/Server/server"
	"../../server/Server"
)
func main(){
	(&Server.Server{
		Network: "tcp",
		Address: "0.0.0.0:9501",
		Clients: make(map[string]*Server.Tcpconnection),
		OnStart: func(server *Server.Server){
			fmt.Println("成功启动服务")
		},
		OnConnect: func(server *Server.Server, client *Server.Tcpconnection) {
			fmt.Println("客户端连接成功")
		},
		OnError: func(err string){
			fmt.Println("[OnError]==",err)
		},
	}).Start()
}