package queue
//An fifo queue
type Queue []int

//push
func (q *Queue) Push(v int) {
	*q = append(*q,v)
}
//pull
func (q *Queue) Pull() int {
	head := (*q)[0]
	*q = (*q)[1:]
	return head
}
//is empty
func (q *Queue) Is_empty() bool {
	return len(*q) == 0
}


func main() {
	
}
