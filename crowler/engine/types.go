package engine

type Resquest struct {
	Url        string
	ParserFunc func([]byte) ParseResult
}

type ParseResult struct {
	Resquests []Resquest
	Items     []interface{}
}

func NilParse([]byte) ParseResult {
	return ParseResult{}
}
