package engine

import (
	"../fetchar"
	"log"
)

func Run(seeds ...Resquest) {
	var requests []Resquest
	for _, r := range seeds {
		requests = append(requests, r)
	}

	for len(requests) > 0 {
		r := requests[0]
		requests = requests[1:]

		log.Printf("Fetching %s", r.Url)
		body, err := fetchar.Fetcher(r.Url)
		if err != nil {
			log.Printf("Fetcher error fetching url %s : %v", r.Url, err)
			continue
		}
		ParseResult := r.ParserFunc(body)
		requests = append(requests, ParseResult.Resquests...)
		for _, item := range ParseResult.Items {
			log.Printf("Got item %s ", item)
		}

	}
}
