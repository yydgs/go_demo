package parser

import (
	"../../engine"
	"regexp"
)

//<th><a href="http://album.zhenai.com/u/1380806998" target="_blank">沉默不语</a></th>
const cityRe = `<a href="(http://album.zhenai.com/u/[0-9]+)"[^>]*>([^<]+)</a>`

func ParserCity(contents []byte) engine.ParseResult {
	re := regexp.MustCompile(cityRe)
	matchs := re.FindAllSubmatch(contents, -1)
	result := engine.ParseResult{}
	for _, m := range matchs {
		result.Items = append(result.Items, "User:"+string(m[2]))
		result.Resquests = append(result.Resquests, engine.Resquest{string(m[1]), ParserProfile})
	}
	return result
}
