package parser

import (
	"../../engine"
	"fmt"
	"regexp"
)

const Re = `<div data-v-8b1eac0c="" class="m-btn purple">(([^<]+))</div>`

func ParserProfile(contents []byte) engine.ParseResult {
	//profile := model.Profile{}
	re := regexp.MustCompile(Re)
	result := engine.ParseResult{}
	match := re.FindAllSubmatch(contents, -1)

	for _, r := range match {
		fmt.Printf("r: %s,%s\n", r[0], r[1])
		result.Items = append(result.Items, r[1])
		//if match != nil {
		//	age,err := strconv.Atoi(string(match[1]))
		//	if err != nil {
		//		//user age is age
		//	}
		//	fmt.Println(age)
	}
	return result
}
