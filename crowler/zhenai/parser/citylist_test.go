package parser

import (
	"../../fetchar"
	"testing"
)

func TestParseCityList(t *testing.T) {
	contents, err := fetchar.Fetcher("http://www.zhenai.com/zhenghun")
	if err != nil {
		panic(err)
	}

	result := ParseCityList(contents)
	//fmt.Printf("%s\n",contents)
	const resultSize = 470
	if len(result.Resquests) != resultSize {
		t.Errorf("result should have %d requests,but had %d", resultSize, len(result.Resquests))
	}

	if len(result.Items) != resultSize {
		t.Errorf("result should have %d requests,but had %d", resultSize, len(result.Items))
	}
	//verify result
}
