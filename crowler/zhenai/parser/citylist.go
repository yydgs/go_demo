package parser

import (
	"../../engine"
	"regexp"
)

const cityListRe = `<a href="(http://www.zhenai.com/zhenghun/[0-9a-z]+)"[^>]*>([^<]+)</a>`

func ParseCityList(contents []byte) engine.ParseResult {
	re := regexp.MustCompile(cityListRe)
	matchs := re.FindAllSubmatch(contents, -1)
	result := engine.ParseResult{}
	limit := 10
	for _, m := range matchs {
		result.Items = append(result.Items, "City:"+string(m[2]))
		result.Resquests = append(result.Resquests, engine.Resquest{string(m[1]), ParserCity})
		limit--
		if limit == 0 {
			break
		}
	}
	return result
}
