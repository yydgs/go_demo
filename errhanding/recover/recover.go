package main

import (
	"fmt"
)

func tryRecover() {
	defer func() {
		r := recover()
		if err,ok := r.(error);ok {
			fmt.Println("error:",err)
		} else {
			panic(fmt.Sprintf("i dont know to do :%v",r))
		}
	}()
	//panic(errors.New("this is a panic!"))
	//b := 0
	//a := 5/b
	//fmt.Println(a)
	panic(123)
}

func main() {
	tryRecover()
}
