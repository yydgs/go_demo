package main

import (
	"./fib"
	"bufio"
	"fmt"
	"os"
)

func tryDefer() {
	defer fmt.Println(2)
	defer fmt.Println(3)
	fmt.Println(1)
	//return
	//panic("error occurred")
	fmt.Println(4)
}

func writeFile(filename string) {
	file,err := os.Create(filename)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	writer := bufio.NewWriter(file)
	defer writer.Flush()

	f := fib.Fibonacci()
	for i:=0; i<20; i++ {
		fmt.Fprintln(writer,f())
	}
}

func main() {
	tryDefer()
	writeFile("fib.txt")
}
