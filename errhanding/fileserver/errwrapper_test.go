package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func errPanic(writer http.ResponseWriter,request *http.Request) error {
	panic(123)
}

func TestErrWrapper(t *testing.T) {
	tests := []struct {
		h appHandler
		code int
		message string
	}{
		{errPanic, 500,""},
	}
	for _,tt :=range tests {
		f := errWrapper(tt.h)
		resp := httptest.NewRecorder()
		request := httptest.NewRequest(http.MethodGet,"",nil)
		f(resp,request)
		all,_ := ioutil.ReadAll(resp.Body)
		body := string(all)
		if resp.Code != tt.code || body != tt.message {
			t.Errorf("except (%d,%s);" +
				"got (%d,%s)",tt.code,tt.message,resp.Code,body)
		}
	}
}
