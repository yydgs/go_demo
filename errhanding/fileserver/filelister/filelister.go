package filelister

import (
	"io/ioutil"
	"net/http"
	"os"
)

func HandlerFileList(writer http.ResponseWriter, request *http.Request) error {
	path := request.URL.Path[len("/list/"):]
	file,ok := os.Open(path)
	if ok != nil {
		//panic(ok)
		//http.Error(writer,ok.Error(),http.StatusInternalServerError)
		//return
		return ok
	}
	defer file.Close()

	all,err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	writer.Write(all)
	return nil
}
