package main

import (
	"fmt"

	"go_demo/downloader/infra"
)

// func retrieve(url string) string {
// 	resp, err := http.Get(url)
// 	if err != nil {
// 		panic(err)
// 	}

// 	defer resp.Body.Close()

// 	bytes, _ := ioutil.ReadAll(resp.Body)
// 	return string(bytes)
// }

func getRetrieve() retrieve {
	return infra.Retrieve{}
}

type retrieve interface {
	Get(string) string
}

func main() {
	// retrieve := infra.Retrieve{}
	var r retrieve = getRetrieve()
	fmt.Println(r.Get("http://www.baidu.com"))

}
